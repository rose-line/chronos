# Chronos #

## Overview ##

Existing platforms are built with a static network connecting microprocessor, radio, clock system, and other components.This static configuration prevents researchers from experimentally validating the trade-offs between the way that clocks are conditioned and distributed, and the performance of the embedded system. In particular, such design decisions have major impact on time synchronization.  An adaptable hardware platform is developed in order to experiment with and develop new wireless time synchronization technologies. With this platform,  researchers can dynamically reconfigure the hardware to perform experiments ranging from low power to high precision time synchronization.  In addition, the hardware allows researchers to directly specify Quality-of-Time (QoT) through software via an FPGA-synthesized Roseline QoT  module.   This  module  is  the  interface  between  the Linux  kernel  and  the  Roseline  QoT  module  which  allows 1)  control  of  the  frequency  via  external  conditioning  circuits, and 2) tuning of delays and jitter in the clock system.

## Architecture ##

![chronos_arch.png](https://bitbucket.org/repo/aXxqRx/images/1859688058-chronos_arch.png =500x)

## Chronos MKIII ##

![chronos.png](https://bitbucket.org/repo/aXxqRx/images/4137545143-chronos.png =500x)

### Update log ###
* MKI:
initial implementation

* MKII:
Add extra compatible BBB IOs
Add additional MCU Programming Interfaces
Change external clock control logic
OpenSDA Fixes
NRF51822 Fixes

* MKIII:
Chronos.SchDoc
- Change BB_PRU0_14 with BB_PRU0_15 to BB Timer 4 and BB Timer 7
- Change 32kHz Controls to SPI CC1200
FPGA.SchDoc
- add FB to VJTAG & VPUMP
In OpenSDA.SchDoc:
- change R41 and R42 to DNP
- change C88 and C89 to 22pF
Misc:
- change reference of C74, C77, C78, C84, C86, C151 from GRM033R60J104KE19D to GRM188R71C104KA01D
- 0.8pF 0201 to 0603
PS.SchDoc:
- Remove P7 - 5V Jumper
- Add Q1, D3, R43 & R44 - Power bypass

* MKIV:
TBA

## Requirements ##

### Additional Hardware ###
* Micro-USB Cable
* [FlashPro Programmer](http://www.microsemi.com/products/fpga-soc/design-resources/programming/flashpro#overview)

### Software ###
* [Libero SoC](http://www.microsemi.com/products/fpga-soc/design-resources/design-software/libero-soc#overview)

Confirmed OS: Windows 7, 8, 8.1, 10
Libero Version: 11.5

Getting Libero working on Ubuntu is non-trivial. First, download these files:

* Libero SOC for Linux - http://www.microsemi.com/products/fpga-soc/design-resources/design-software/libero-soc#downloads
* Linux License Server Daemon - http://www.microsemi.com/products/fpga-soc/design-resources/licensing#downloads
* FlashPro Express - http://soc.microsemi.com/download/reg/download.aspx?p=f%3dFlashProv11_6_LIN
* Example firmware for the Igloo Nano board - http://www.microsemi.com/products/fpga-soc/design-resources/dev-kits/igloo/igloo-nano-starter-kit#documents 

You will also need to request a free license from this website, which is bound to your Ethernet MAC. It will be emailed to you within 45 minutes. You'll need to edit a few fields in the license file, such as the hostname and path to the license server binaries, before you use it. 
http://www.microsemi.com/products/fpga-soc/design-resources/licensing

The Libero release includes some udev_install script that does a `locate lsusb` to try and find the binary. this doesn't work if you have a rootfs lurking around with a cross-compiled lsusb version. I'd recommend skipping this script and just creating the file ```/etc/udev/rules.d/90-microsemi.rules``` with the following content:

```
SUBSYSTEM=="usb", ATTRS{idProduct}=="2004", ATTRS{idVendor}=="1514", MODE="0660", GROUP="plugdev", SYMLINK+="FlashPro3"
SUBSYSTEM=="usb", ATTRS{idProduct}=="6001", ATTRS{idVendor}=="0403", MODE="0660", GROUP="plugdev", SYMLINK+="FTDI232"
```

Here are some scripts that may help you (you will need to change the paths)

Run this before installing...

```
#!/bin/bash
sudo apt-get install libmotif3 libmotif-dev libmotif4:i386 nspluginwrapper lib32z1 libc6-i386 rpcbind xfonts-100dpi xfonts-75dpi ksh lsb
echo 'OPTIONS="-w -i"' | sudo tee /etc/default/rpcbind
sudo service rpcbind restart
sudo mkdir /usr/tmp
sudo chmod uga+rwx /usr/tmp
echo "*SystemFontSpec:-adobe-utopia-bold-i-normal--0-0-0-0-p-0-adobe-standard" > font_tmp.txt
echo "*SystemFont:-adobe-utopia-bold-i-normal--0-0-0-0-p-0-adobe-standard" >> font_tmp.txt
xrdb -merge font_tmp.txt
rm font_tmp.txt
```

Use this script to run Libero SOC:

```
#!/bin/bash
export LOCALE=C
export LD_LIBRARY_PATH=/usr/lib/i386-linux-gnu/
export LIBERO_INSTALLED_DIR=/home/asymingt/Applications/microsemi/libero
export LIBERO_LICENSE_DIR=/home/asymingt/Applications/microsemi/licensing
PATH=$LIBERO_INSTALLED_DIR/Libero/bin:$PATH;
PATH=$LIBERO_INSTALLED_DIR/Synplify/bin:$PATH;
PATH=$LIBERO_INSTALLED_DIR/Model/modeltech/linuxacoem:$PATH;
export PATH
export LM_LICENSE_FILE=1702@localhost
export SNPSLMD_LICENSE_FILE=1702@localhost
$LIBERO_LICENSE_DIR/lmgrd -c $LIBERO_LICENSE_DIR/License.dat -l $LIBERO_LICENSE_DIR/License.log
libero
```

Use this script to run FlashPro Express:

```
#!/bin/bash
export LOCALE=C
export LD_LIBRARY_PATH=/usr/lib/i386-linux-gnu/
export FLASHPRO_DIR=/home/asymingt/Applications/microsemi/flashpro
PATH=$FLASHPRO_DIR/FlashPro/bin:$PATH;
export PATH
FPExpress
```

## Programming the FPGA with Prebuilt Project through Libero ##

** Plug in the Chronos **

![chronos_pwron.png](https://bitbucket.org/repo/aXxqRx/images/4010841496-chronos_pwron.png =500x)

PWR - The device is powered on with the external power.

CHRG - The device is charging the battery.

** Plug in the programmer **

![chronos_fpga_programming.png](https://bitbucket.org/repo/aXxqRx/images/3943617339-chronos_fpga_programming.png =500x)

Download the example “Chronos_CC2520” project from Downloads.

Unzip the archive. Start up Libero SoC and open the example project (Project - Open Project - <Name of the Project>.prjx - Open)

** Program the device **

![libero_program_device.PNG](https://bitbucket.org/repo/aXxqRx/images/1540711859-libero_program_device.PNG =500x)

Under Design Flow (Left panel) - Program Design - Program Device

## Programming the FPGA with Prebuilt Project through FlashPro ##



## Custom FPGA ##

[This is a good tutorial for Libero SOC with SmartFusion.](http://wiesel.ece.utah.edu/schmid/teaching/ece5780/ece5780-s14-lab/lab1/)
However, the settings will be different for Chronos (see below).

![libero_chronos_settings.PNG](https://bitbucket.org/repo/aXxqRx/images/2340565195-libero_chronos_settings.PNG =500x)

** Create new project **

![libero_new_proj.PNG](https://bitbucket.org/repo/aXxqRx/images/2886253361-libero_new_proj.PNG =500x)

** Create new SmartDesign **

![libero_new_sd.PNG](https://bitbucket.org/repo/aXxqRx/images/2127538210-libero_new_sd.PNG =500x)

Note: Make sure your main module is root! Under design hierarchy - right click on your main module - set as root.

## RDDXO ##

## BeagleBone Black: CC2520 Module ##


## BeagleBone Black: CC1200 Module ##

Download the FPGA for CC1200 Module. Boot up the QoT Stack.

** Initialize cc1200 LKM network interface **
```
./cc1200_init.sh wlan0 [device number]
```

** Run example program **

```
./cc1200 wlan0 [device number] [1 for tx and 0 for rx]
```

## Beaglebone Black: RDDXO ##

### Additional Chronos Documentations ###
Under source/chronos/

## Freescale ##


2.0 Install the J-Link Software Pack

https://www.segger.com/jlink-software.html

2.1 Jump Start Your Design with the Kinetis SDK!

The Kinetis Software Development Kit (SDK) is complimentary and includes full source code under a permissive open-source license for all hardware abstraction and peripheral driver software.
Click below to download the SDK Release appropriate for your computer's operating system.

https://nxp.flexnetoperations.com/control/frse/download?agree=Accept&element=6373617


2.2 Install Your Toolchain
NXP offers a complimentary toolchain called Kinetis Design Studio (KDS).

https://nxp.flexnetoperations.com/control/frse/product?entitlementId=172047007&lineNum=1&authContactId=84999767&authPartyId=83658857


2.3 PC Configuration
Many of the example applications output data over the MCU UART so you'll want to make sure that the driver for the board's virtual COM port is installed. Before you run the driver installer, you MUST have the board plugged in to your PC.

http://developer.mbed.org/media/downloads/drivers/mbedWinSerial_16466.exe

 
With the serial port driver installed, run your favorite terminal application to view the serial output from the MCU's UART. Configure the terminal to 115200 baud rate, 8 data bits, no parity and 1 stop bit. To determine the port number of the FRDM-K22F's virtual COM port, open the device manager and look under the "Ports" group.


How to Make a New Project

File > New
Enter Project Name
[Next >]
Dropdown:  Processors > Kinetis K > MK20 > MK22F  (120MHz) > MK22FN512xxx12
[Finish]

### Directory Structure ###

gerbers - gerbers to send to Fab house
BOM - Bill of Materials (buy these!)
images - advertising

schematic - schematic of the board
fgpa - programmer guide's excel workbooks

### MISC ###

NOTE:
Instead of regular .100" 2x23 header with
https://www.adafruit.com/products/706
or
https://www.sparkfun.com/products/12790

Order SP-2U1+ from MiniCircuits.com